public class TestGeneric {
    public static void main(String[] args){
        Generic<String> e1 = new Generic<>("Samuel Jackson");
        Generic<String> e2 = new Generic<>(" cashier");
        Generic<Integer> salary = new Generic<>( 2000);

        System.out.println("My name is " +e1.get() + ".");
        System.out.println("I work as " + e2.get() + "at Plaza hotel" + ".");
        System.out.println("My salary is valued somewhere around " + salary.get() + " dollars" + ".");
    }
    }

